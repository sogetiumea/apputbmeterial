using System;
using TestApp.Core;

namespace TestApp.iOS
{
    class DeviceInfoHelper_iOS : IDeviceInfoHelper
    {
        public string GetDeviceName()
        {
            return "iOS";
        }
    }
}