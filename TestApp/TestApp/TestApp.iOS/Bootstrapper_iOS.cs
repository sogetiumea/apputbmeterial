﻿
using Autofac;
using TestApp.Core;

namespace TestApp.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            // Initialize the ComponentContainer
            ComponentContainer.Initialize();

            // Register common types
            App.RegisterTypes();

            // Register device specific types
            RegisterTypes();

            // Tell ComponentContainer to build the internal container
            ComponentContainer.Build();

        }

        public static void RegisterTypes()
        {
            ComponentContainer.Register<IDeviceInfoHelper, DeviceInfoHelper_iOS>();
        }
    }
}
