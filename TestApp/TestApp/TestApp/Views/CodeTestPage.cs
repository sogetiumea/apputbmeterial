﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TestApp
{
    public class CodeTestPage : ContentPage
    {
        public CodeTestPage()
        {
            //Content = new StackLayout
            //{
            //    VerticalOptions = LayoutOptions.Center,
            //    Children = {
            //        new Label {
            //            Text = "Hello from Code! (1)",
            //            HorizontalTextAlignment = TextAlignment.Center
            //        }
            //    }
            //};

            var label = new Label();
            label.Text = "Hello from Code! (2)";
            label.HorizontalTextAlignment = TextAlignment.Center;

            var layout = new StackLayout();
            layout.VerticalOptions = LayoutOptions.Center;

            layout.Children.Add(label);

            Content = layout;
        }
    }
}
