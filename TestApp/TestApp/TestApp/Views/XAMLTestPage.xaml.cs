﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TestApp
{
    public partial class XAMLTestPage : ContentPage
    {
        public XAMLTestPage()
        {
            InitializeComponent();

            MyLabel.Text = "Hello from CodeBehind";
            MyButton.Clicked += MyButton_Clicked;

        }

        private void MyButton_Clicked(object sender, EventArgs e)
        {
            MyLabel.Text = "Button was clicked!";
        }
    }
}
