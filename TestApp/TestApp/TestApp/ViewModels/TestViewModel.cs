﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestApp.Core;
using Xamarin.Forms;

namespace TestApp
{
    public class TestViewModel : BaseViewModel
    {
        public TestViewModel()
        {
            IMessageManager mm = ComponentContainer.Resolve<IMessageManager>();
            HelloMessage = mm.GetMessage();
        }

        private void UpdateMessage()
        {
            HelloMessage = "Message is updated!";
        }

        private async Task GotoNextpage()
        {
            var viewModel = new AnotherViewModel();
            viewModel.AnotherMessage = "This is another message!";
            var page = ViewFactory.CreatePage<AnotherViewModel>(viewModel);

            //var viewModel = new AnotherViewModel();
            //viewModel.AnotherMessage = "This is another message!";
            //var page = new AnotherPage();
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            await Navigation.PushAsync(page);
        }

        public string HelloMessage { get; set; }

        public ICommand UpdateMessageCommand
        {
            get
            {
                return new Command(() => UpdateMessage());
            }
        }

        public ICommand ContinueCommand
        {
            get
            {
                return new Command(async () => await GotoNextpage());
            }
        }

    }
}
