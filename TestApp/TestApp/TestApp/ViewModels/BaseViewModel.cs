﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace TestApp
{
    [ImplementPropertyChanged]
    public class BaseViewModel
    {
        public INavigation Navigation { get; set; }
    }
}
