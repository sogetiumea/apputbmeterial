﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using TestApp.Core;

namespace TestApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new CodeTestPage();

            //var viewModel = new TestViewModel();
            //var page = new TestPage();
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            RegisterViews();

            var page = ViewFactory.CreatePage<TestViewModel>();

            MainPage = new NavigationPage(page);
        }

        private void RegisterViews()
        {
            ViewFactory.Initialize();
            ViewFactory.Register<TestViewModel, TestPage>();
            ViewFactory.Register<AnotherViewModel, AnotherPage>();
        }

        public static void RegisterTypes()
        {
            // Register Core and Forms types.

            ComponentContainer.Register<IMessageManager, MessageManager>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
