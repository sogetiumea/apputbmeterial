﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Core
{
    public class MessageManager : IMessageManager
    {
        public MessageManager(IDeviceInfoHelper deviceInfoHelper)
        {
            this.deviceInfoHelper = deviceInfoHelper;
        }

        public string GetMessage()
        {
            return "Hello from " + deviceInfoHelper.GetDeviceName();
        }

        private IDeviceInfoHelper deviceInfoHelper;
    }
}
