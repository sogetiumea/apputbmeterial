
using TestApp.Core;

namespace TestApp.Droid
{
    public class Bootstrapper_Droid
    {
        public static void Initialize()
        {
            // Initialize the ComponentContainer
            ComponentContainer.Initialize();

            // Register common types
            App.RegisterTypes();

            // Register device specific types
            RegisterTypes();

            // Tell ComponentContainer to build the internal container
            ComponentContainer.Build();
        }

        public static void RegisterTypes()
        {
            ComponentContainer.Register<IDeviceInfoHelper, DeviceInfoHelper_Droid>();
        }
    }
}