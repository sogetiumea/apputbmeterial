using System;
using TestApp.Core;

namespace TestApp.Droid
{
    class DeviceInfoHelper_Droid : IDeviceInfoHelper
    {
        public string GetDeviceName()
        {
            return "Android";
        }
    }
}